# from xml.dom import minidom
import math

import libVRange

class Rect:
  def __init__(self):
    self.x0 = None
    self.y0 = None
    self.x1 = None
    self.y1 = None
    self.svgElement = None

  def to_s(self):
    return ( "%04dx%04d+%04d+%04d" % ( self.getWidth(), self.getHeight(), self.x0, self.y0 ) )

  def from_s(self, s):
    toks = s.split("+")
    self.y0 = int(toks.pop())
    self.x0 = int(toks.pop())
    toks = toks.pop().split("x")
    self.y1 = self.y0 + int(toks.pop())
    self.x1 = self.x0 + int(toks.pop())
    return self

  def setXYWH_i(self, x, y, w, h):
    self.x0 = x
    self.y0 = y
    self.x1 = x + w
    self.y1 = y + h
    return self

  def setXYWH_s(self, x, y, w, h):
    return self.setXYWH_i(int(x), int(y), int(w), int(h))

  def setX0Y0X1Y1_i(self, x0, y0, x1, y1):
    self.x0 = x0
    self.x1 = x1
    self.y0 = y0
    self.y1 = y1
    return self

  def setX0Y0X1Y1_s(self, x0, x1, y0, y1):
    return self.setX0Y0X1Y1_i(int(x0), int(y0), int(x1), int(y1))

  def getCenterX(self):
    return (0.5 * (self.x0 + self.x1))

  def getCenterY(self):
    return (0.5 * (self.y0 + self.y1))

  def getCenterXY(self):
    return [self.getCenterX(), self.getCenterY()]

  def getWidth(self):
    return (self.x1 - self.x0)

  def getHeight(self):
    return (self.y1 - self.y0)

  def getVRangeX(self):
    return libVRange.VRange().fromPair(self.x0, self.x1)

  def getVRangeY(self):
    return libVRange.VRange().fromPair(self.y0, self.y1)

  def getWH(self):
    return [self.getWidth(), self.getHeight()]

  def createSvgElement(self, root):
    elm = root.createElement("rect")
    elm.setAttribute("x", str(self.x0))
    elm.setAttribute("y", str(self.y0))
    elm.setAttribute("width", str(self.getWidth()))
    elm.setAttribute("height", str(self.getHeight()))
    self.svgElement = elm
    return elm

  def relateSvgElement(self, elm):
    self.svgElement = elm
    return self

  def applyToSvgElement(self, elm):
    elmSvg.setAttribute("x", str(self.x0))
    elmSvg.setAttribute("y", str(self.y0))
    elmSvg.setAttribute("width", str(self.getWidth()))
    elmSvg.setAttribute("height", str(self.getHeight()))
    return elmSvg

  def loadFromSvgElement(self, elm):
    if elm is None:
      elm = self.svgElement
    self.setXYWH_s(elm.getAttribute("x"), elm.getAttribute("y"), elm.getAttribute("width"), elm.getAttribute("height"))
    self.relateSvgElement(elm)
    return self

  def createToInclude(self, another):
    newRect = Rect()
    newRect.x0 = min([self.x0, another.x0])
    newRect.x1 = max([self.x1, another.x1])
    newRect.y0 = min([self.y0, another.y0])
    newRect.y1 = max([self.y1, another.y1])
    return newRect

  def updateToInclude(self, another):
    self.x0 = min([self.x0, another.x0])
    self.x1 = max([self.x1, another.x1])
    self.y0 = min([self.y0, another.y0])
    self.y1 = max([self.y1, another.y1])
    return self

  def hasOverlapX(self, another):
    return ((another.x1 - self.x0) * (another.x0 - self.x1) <= 0) 

  def hasOverlapY(self, another):
    return ((another.y1 - self.y0) * (another.y0 - self.y1) <= 0) 

  def hasOverlapWith(self, another):
    if self.hasOverlapX(another) and self.hasOverlapY(another):
      return True
    else:
      return False

  def coverCenterX(self, another):
    x2 = another.getCenterX()
    return (self.x0 <= x2 and x2 <= self.x1)

  def coverCenterY(self, another):
    y2 = another.getCenterY()
    return (self.y0 <= y2 and y2 <= self.y1)

  def coverCenterOf(self, another):
    return self.coverCenterX(another) and self.coverCenterY(another)

  def getDistanceXFrom(self, another):
    return abs(self.getCenterX() - another.getCenterX())

  def getDistanceYFrom(self, another):
    return abs(self.getCenterY() - another.getCenterY())

  def getDistanceFrom(self, another):
    return math.sqrt( self.getDistanceXFrom(another) ** 2 + self.getDistanceYFrom(another) ** 2 )

  def getVectorTo(self, another):
    return [ another.getCenterX() - self.getCenterX(), another.getCenterY() - self.getCenterY() ]

  def getVectorFrom(self, another):
    return [ self.getCenterX() - another.getCenterX(), self.getCenterY() - another.getCenterY() ]

  def isLeftOf(self, another):
    if (self.getCenterX() <= another.getCenterX()):
      return True
    else:
      return False

  def isLowerThan(self, another):
    if (self.getCenterY() <= another.getCenterY()):
      return True
    else:
      return False

  def createGapRectWith(self, another, coverH = False, coverV = False):
    if self.hasOverlapWith(another):
      return None
    elif not self.hasOverlapX(another) and not self.hasOverlapY(another):
      return None

    newRect = Rect()
    if (coverH):
      newRect.x0 = min([self.x0, another.x0])
      newRect.x1 = max([self.x1, another.x1])
    elif (self.isLeftOf(another)):
      newRect.x0 = self.x1
      newRect.x1 = another.x0
    else:
      newRect.x0 = another.x1
      newRect.x1 = self.x0

    if (coverV):
      newRect.y0 = min([self.y0, another.y0])
      newRect.y1 = max([self.y1, another.y1])
    elif (self.isLowerThan(another)):
      newRect.y0 = self.y1
      newRect.y1 = another.y0
    else:
      newRect.y0 = another.y1
      newRect.y1 = self.y0

    return newRect

  def createGapRectHWith(self, another):
    return self.createGapRectWith(another, True, False)

  def createGapRectHWith(self, another):
    return self.createGapRectWith(another, False, True)

  def createRectWithMargin(self, marginX = 0, marginY = 0):
    newRect = Rect()
    newRect.x0 = self.x0 - marginX
    newRect.x1 = self.x1 + marginX
    newRect.y0 = self.y0 - marginY
    newRect.y1 = self.y1 + marginY
    return newRect

  def getNearestNeighbor(self, others, direction):
    d = direction.lower()

    if d == "upper":
      cndds = filter(lambda another: self.hasOverlapX(another) and self.isLowerThan(another), others)
      if len(cndds) == 0:
        return None
      cndds.sort(key=lambda another: self.getDistanceFrom(another))
      return cndds.pop(0)

    elif d == "lower":
      cndds = filter(lambda another: self.hasOverlapX(another) and another.isLowerThan(self), others)
      if len(cndds) == 0:
        return None
      cndds.sort(key=lambda another: self.getDistanceFrom(another))
      return cndds.pop(0)

    elif d == "left":
      cndds = filter(lambda another: self.hasOverlapY(another) and another.isLeftOf(self), others)
      if len(cndds) == 0:
        return None
      cndds.sort(key=lambda another: self.getDistanceFrom(another))
      return cndds.pop(0)

    elif d == "right":
      cndds = filter(lambda another: self.hasOverlapY(another) and self.isLeftOf(another), others)
      if len(cndds) == 0:
        return None
      cndds.sort(key=lambda another: self.getDistanceFrom(another))
      return cndds.pop(0)

class RectSet:
  def __init__(self):
    self.rects = set([])
    self.frame = Rect()
    self.svgElement = None

  def getRectList(self):
    return list(self.rects)

  def appendRect(self, r):
    self.rects |= set([r])
    return self

  def appendRects(self, rs):
    self.rects |= set(rs)
    return self

  def appendSvgElement(self, e):
    self.appendRect(Rect().loadFromSvgElement(e))
    return self

  def appendSvgElements(self, es):
    for e in es:
      self.appendSvgElement(e)
    return self

  def updateFrame(self):
    if len(self.rects) == 0:
      return self

    x0 = min(map(lambda r: r.x0, self.rects))
    x1 = max(map(lambda r: r.x1, self.rects))
    y0 = min(map(lambda r: r.y0, self.rects))
    y1 = max(map(lambda r: r.y1, self.rects))
    self.frame.setX0Y0X1Y1_i(x0, y0, x1, y1)
    return self

  def createSvgElement(self, root):
    if root is None:
      root = list(self.rects)[0].svgElement.ownerDocument
    elm = root.createElement("rect")
    elm.setAttribute("x", str(self.frame.x0))
    elm.setAttribute("y", str(self.frame.y0))
    elm.setAttribute("width", str(self.frame.getWidth()))
    elm.setAttribute("height", str(self.frame.getHeight()))
    self.svgElement = elm
    return elm
