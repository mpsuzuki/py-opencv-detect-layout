class VRange:
  def __init__(self):
    self.min = None
    self.max = None

  def getCenter(self):
    return 0.5 * (self.min + self.max)

  def fromPair(self, min, max):
    self.min = min
    self.max = max
    return self

  def fromList(self, list):
    self.min = min(list)
    self.max = max(list)
    return self

  def toList(self):
    return [self.min, self.max]

  def to_s(self):
    return str(self.min) + "..." + str(self.max)

  def covers(self, v):
    if v.__class__.__name__ == "VRange":
      return (self.min <= v.min and v.max <= self.max)
    else: 
      return (self.min <= v and v <= self.max)

  def isGT(self, v):
    if v.__class__.__name__ == "VRange":
      return (v.max < self.min)
    else:
      return (v < self.min)

  def isGE(self, v):
    if v.__class__.__name__ == "VRange":
      return (v.max <= self.min)
    else:
      return (v <= self.min)

  def isLT(self, v):
    if v.__class__.__name__ == "VRange":
      return (self.max < v.min)
    else:
      return (self.max < v)

  def isLE(self, v):
    if v.__class__.__name__ == "VRange":
      return (self.max <= v.min)
    else:
      return (self.max <= v)
