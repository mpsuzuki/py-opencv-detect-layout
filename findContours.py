#!/usr/bin/env python

import sys
import os.path
import numpy
import cv2
import itertools
import re
import mygetopts
import json
import libRect
import libVRange
import math
import base64

from xml.dom import minidom
import extendDOM

Opts = {}
Opts["circle"] = False
Opts["args"] = []
Opts["blur-x"] = 0
Opts["blur-y"] = 0
Opts["page-number-height"] = "0..0"
Opts["glyph-height"] = "0..0"
Opts["hint-rect"] = []
Opts["threshold"] = 127
Opts["embed"] = False
Opts["output"] = None

Opts = mygetopts.getOpts(Opts, sys.argv, ["circle", "embed"])

jsDoc = minidom.parseString("<opts><!-- " + json.dumps(Opts) + " --></opts>")

Opts["pathImg"] = Opts["args"].pop(0)

for k in [ "blur-x", "blur-y" ]:
  Opts[k] = 1 + 2 * int(Opts[k])

for k in [ "page-number-height", "page-number-width",
           "glyph-height", "glyph-width",
           "vline-height", "vline-width", "vline-inked-width",
           "note-height", "note-width"
         ]:
  if k in Opts.keys():
    Opts[k] = Opts[k].split("..")
    Opts[k] = libVRange.VRange().fromPair(int(Opts[k][0]), int(Opts[k][1]) + 1)

if "hint-svg" in Opts.keys() and os.path.isfile(Opts["hint-svg"]):
  Opts["hint-rect"] = filter(lambda elm: elm.hasClass("corrected") , \
                             minidom.parse(Opts["hint-svg"]).documentElement.getElementsByTagName("rect"))

img = cv2.imread(Opts["pathImg"])
for elmHint in Opts["hint-rect"]:
  x1 = int(elmHint.getAttribute("x"))
  y1 = int(elmHint.getAttribute("y"))
  x2 = x1 + int(elmHint.getAttribute("width"))
  y2 = y1 + int(elmHint.getAttribute("height"))
  cv2.rectangle(img, (x1, y1), (x2, y2), (255,255,255), -1)

hImg, wImg = img.shape[:2]
imgGray0 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
v, imgGray = cv2.threshold(imgGray0, float(Opts["threshold"]), 255, cv2.THRESH_BINARY)
# print(imgGray)

if Opts["blur-x"] > 1 or Opts["blur-y"] > 1:
  kernel = numpy.ones((Opts["blur-y"], Opts["blur-x"]), numpy.float32)/(Opts["blur-x"] * Opts["blur-y"])
  imgGrayInversed = cv2.filter2D((255 - imgGray), -1, kernel)
else:
  imgGrayInversed = (255 - imgGray)


# cv2.imwrite("sample.png", imgGrayInversed)
contoursRaw, hier = cv2.findContours( imgGrayInversed.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# contours = [cv2.approxPolyDP(aCnt, 3, True) for aCnt in contoursRaw]

svgSkel = """
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:html="http://www.w3.org/1999/xhtml">
</svg>
"""

if not Opts["embed"]:
  svgSkel = "<?xml-stylesheet type=\"text/css\" href=\"svg-color.css\" ?>\n" + svgSkel

svgDoc = minidom.parseString(svgSkel)
svgRoot = svgDoc.documentElement
svgRoot.setAttribute("width", str(wImg))
svgRoot.setAttribute("height", str(hImg))

if Opts["embed"]:
  fh = open("svg-color.css")
  svgColorCssText = fh.read()
  fh.close
  svgDoc.createElement("style") \
        .appendTo(svgRoot) \
        .appendChild(svgDoc.createTextNode("\n" + svgColorCssText + "\n"))

svgRoot.appendChild(jsDoc.documentElement.childNodes[0])

elmImg = svgDoc.createElement("image")
svgRoot.appendChild(elmImg)
elmImg.setAttribute("width", str(wImg))
elmImg.setAttribute("height", str(hImg))
if not Opts["embed"]:
  elmImg.setAttribute("xlink:href", Opts["pathImg"])
else:
  fh = open(Opts["pathImg"], "rb")
  elmImg.setAttribute("xlink:href", "data:image/png;base64," + base64.b64encode(fh.read()))
  fh.close

for idx, aCnt in enumerate(contoursRaw):
  if hier[0][idx][3] != -1:
    continue

  if Opts["circle"]:
    (cntX, cntY), cntRad = cv2.minEnclosingCircle(aCnt)
    elmCont = svgDoc.createElement("circle")
    elmCont.setAttribute("cx", str(cntX))
    elmCont.setAttribute("cy", str(cntY))
    elmCont.setAttribute("r", str(cntRad))

  else:
    rectX, rectY, rectW, rectH = cv2.boundingRect(aCnt)

    maxInkedWidth = None
    avgInkedWidth = None
    inkedWidths = []
    if Opts["note-width"].isGT(rectW) and rectW < rectH:
      for iy in range(rectY, rectY + rectH):
        iwLast = 0
        for iw in range(1, rectW):
          tested = []
          for ix in range(rectX, rectX + rectW - iw):
            tested.append( all( map(lambda pxl: pxl < 127, imgGray[iy, ix:ix + iw]) ) )

          if any( tested ):
            iwLast = iw + 1

        inkedWidths.append(iwLast)
        # print(["inkedWidths:", inkedWidths])

      maxInkedWidth = max(inkedWidths)
      avgInkedWidth = sum(inkedWidths) * 1.0 / rectH

    elmCont = svgDoc.createElement("rect")
    elmCont.setAttribute("x", str(rectX))
    elmCont.setAttribute("y", str(rectY))
    elmCont.setAttribute("width", str(rectW))
    elmCont.setAttribute("height", str(rectH))

    if 0 < len(inkedWidths):
      elmCont.setAttribute("data-avg-inked-width", ("%3.2f" % avgInkedWidth))

    if Opts["page-number-height"].covers(rectH) and ("page-number-width" not in Opts.keys() or Opts["page-number-width"].covers(rectW)):
      elmCont.addClass("page-number")
    elif Opts["glyph-height"].covers(rectH) and ("glyph-width" not in Opts.keys() or Opts["glyph-width"].covers(rectW)):
      elmCont.addClass("glyph")
    elif ("note-height" in Opts.keys() and Opts["note-height"].covers(rectH)) and \
         ("note-width" in Opts.keys() and Opts["note-width"].covers(rectW)):
      elmCont.addClass("note")
    elif avgInkedWidth and avgInkedWidth < 0.5 * rectW and Opts["vline-width"].isLT(rectW):
      elmCont.addClass("none")
    elif Opts["vline-width"].covers(avgInkedWidth) and (rectW < rectH) :
      elmCont.addClass("vline").addClass("border")
    elif rectW > (2 * Opts["glyph-width"].max) and Opts["glyph-height"].isGT(rectH):
      elmCont.addClass("hline")
    elif "vline-width" in Opts.keys() and Opts["vline-width"].covers(rectW) and (rectW < rectH):
      elmCont.addClass("vline")
    elif rectW > (2 * Opts["glyph-width"].max) and rectH > (2 * Opts["glyph-height"].min):
      elmCont.addClass("none")
    elif "vline-inked-width" in Opts.keys() and avgInkedWidth and avgInkedWidth < Opts["vline-inked-width"].max and (3 * rectW < rectH):
      elmCont.addClass("vline").addClass("border")
    elif rectW < rectH:
      elmCont.addClass("tall")

  svgRoot.appendChild(elmCont)

for elmHint in Opts["hint-rect"]:
  svgRoot.appendChild(elmHint)

elmGlyphs = filter(lambda elm: elm.hasClass("glyph"),
                   svgDoc.getElementsByTagName("rect"))
elmPageNumbers = filter(lambda elm: elm.hasClass("page-numbers"), \
                        svgDoc.getElementsByTagName("rect"))
elmNotes = filter(lambda elm: elm.hasClass("note"), \
                  svgDoc.getElementsByTagName("rect"))

# ---
# detect the shadow at the inner hinge.

rectSetGlyphAndNote = libRect.RectSet() \
                             .appendRects(map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmGlyphs)) \
                             .appendRects(map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmNotes)) \
                             .updateFrame()

yFloor = rectSetGlyphAndNote.frame.y1	# in SVG coordination, the lower point has bigger y
yRoof = rectSetGlyphAndNote.frame.y0	# in SVG coordination, the higher point has smaller y

xHingeRoof = None
xHingeRoofL = None
xHingeRoofR = None
xHingeFloor = None
xHingeFloorL = None
xHingeFloorR = None

if yFloor is not None and yRoof is not None:
  hUnderFloor = hImg - yFloor
  hOverRoof = yRoof

  fillUnderFloor = [False] * wImg
  fillOverRoof = [False] * wImg

  for ix in range(int(wImg * 0.3), int(wImg * 0.7)):
    nFilledPixelUnderFloor = 0
    for iy in range(yFloor, hImg):
      if imgGray[iy, ix] < 127:
        nFilledPixelUnderFloor += 1
    fillUnderFloor[ix] = ( nFilledPixelUnderFloor > (hUnderFloor * 0.5))

    nFilledPixelOverRoof = 0
    for iy in range(0, hOverRoof):
      if imgGray[iy, ix] < 127:
        nFilledPixelOverRoof += 1
    fillOverRoof[ix] = (nFilledPixelUnderFloor > (hOverRoof * 0.5))

  def testHinge(ix, hingeWidth, filledArray):
    return all( filledArray[(ix - (hingeWidth / 2)):(ix + (hingeWidth / 2))] )

  hingeWidth = 4

  rng = range(hingeWidth / 2, wImg - (hingeWidth / 2) + 1)

  for ix in rng:
    if xHingeRoofL is None:
      if testHinge(ix, hingeWidth, fillOverRoof):
        xHingeRoofL = ix

    if xHingeFloorL is None:
      if testHinge(ix, hingeWidth, fillUnderFloor):
        xHingeFloorL = ix

    if xHingeRoofL is not None and xHingeFloorL is not None:
      break

  rng.reverse()

  for ix in rng:
    if xHingeRoofR is None:
      if testHinge(ix, hingeWidth, fillOverRoof):
        xHingeRoofR = ix

    if xHingeFloorR is None:
      if testHinge(ix, hingeWidth, fillUnderFloor):
        xHingeFloorR = ix

    if xHingeRoofR is not None and xHingeFloorR is not None:
      break

  # xHinge = (xHingeL + xHingeR) / 2
  if xHingeRoofL is not None and xHingeRoofR is not None:
    xHingeRoof = (xHingeRoofL + xHingeRoofR) / 2
  elif xHingeRoofL is not None:
    xHingeRoof = xHingeRoofL
  elif xHingeRoofR is not None:
    xHingeRoof = xHingeRoofR

  if xHingeFloorL is not None and xHingeFloorR is not None:
    xHingeFloor = (xHingeFloorL + xHingeFloorR) / 2
  elif xHingeFloorL is not None:
    xHingeFloor = xHingeFloorL
  elif xHingeFloorR is not None:
    xHingeFloor = xHingeFloorR

  # print([xHingeRoofL, xHingeRoofR, xHingeFloorL, xHingeFloorR])
  # print([hOverRoof, hUnderFloor, ix])

# ---
# filter-out the circles in elmNotes

def testContentIsCircle(elm, img):
  rect = libRect.Rect().loadFromSvgElement(elm)
  w = rect.getWidth()
  h = rect.getHeight()

  #print("*** testContentIsCircle: " + rect.to_s())

  cropImg = img[rect.y0:(rect.y1+1), rect.x0:(rect.x1 + 1)].copy()
  #print("===========================================================================")
  #print(cropImg)
  numInkedPixelOrg = 0
  for iy in range(0, h+1):
    for ix in range(0, w+1):
      if cropImg[iy, ix] < 127:
        numInkedPixelOrg += 1

  # print("---------------------------------------------------------------------------")

  cx = int(w * 0.5)
  cy = int(h * 0.5)
  r = int((cx + cy) * 0.5)
  cv2.circle(cropImg, (cx, cy), r, 255, abs(w - h) + 6)
  #print(cropImg)
  #print("===========================================================================")
  numInkedPixelMasked = 0
  for iy in range(0, h+1):
    for ix in range(0, w+1):
      if cropImg[iy, ix] < 127:
        numInkedPixelMasked += 1

  #print(str(numInkedPixelOrg) + " -> " + str(numInkedPixelMasked))
  if numInkedPixelMasked < 10:
    return True

  return False

for e in elmNotes:
  # if testContentIsCircle(e, imgGrayInversed):
  if testContentIsCircle(e, imgGray):
    e.removeClass("note").addClass("circle")

# -----------------------------------------------------------------------------------

xHinges = filter(lambda v: v is not None, [xHingeFloorL, xHingeFloorR, xHingeRoofL, xHingeRoofR])

rectSetForPageR = libRect.RectSet()
rectSetForPageL = libRect.RectSet()
if len(xHinges) > 0:
  for e in svgDoc.getElementsByTagName("rect"):
    if e.hasClass("noteset-frame"):
      continue
    r = libRect.Rect().loadFromSvgElement(e)
    if r.x1 < min(xHinges):
      e.addClass("page-L")
    elif r.x0 > max(xHinges):
      e.addClass("page-R")

  rectSetForPageR.appendSvgElements( \
                    filter(lambda e: e.hasClass("page-R") and e.hasAnyOfClasses(["note", "glyph"]), \
                           svgDoc.getElementsByTagName("rect")) \
                  ).updateFrame()
  rectSetForPageL.appendSvgElements( \
                    filter(lambda e: e.hasClass("page-L") and e.hasAnyOfClasses(["note", "glyph"]), \
                           svgDoc.getElementsByTagName("rect")) \
                  ).updateFrame()

if len(rectSetForPageR.rects) > 0:
  yRoofPageR = rectSetForPageR.frame.y0
  yFloorPageR = rectSetForPageR.frame.y1
  if xHingeRoofR is not None and yRoofPageR is not None:
    elmSpaceOverRoofPageR = svgDoc.createElement("rect") \
                                  .addClass("outside") \
                                  .setAttrByDict({"x": str(xHingeRoofR), \
                                                  "y": "0", \
                                                  "width": str(wImg - xHingeRoofR), \
                                                  "height": str(yRoofPageR)}) \
                                  .appendTo(svgRoot)
  if xHingeFloorR is not None and yFloorPageR is not None:
    elmSpaceUnderFloorPageR = svgDoc.createElement("rect") \
                                    .addClass("outside") \
                                    .setAttrByDict({"x": str(xHingeFloorR), \
                                                    "y": str(yFloorPageR), \
                                                    "width": str(wImg - xHingeFloorR), \
                                                    "height": str(hImg - yFloorPageR)}) \
                                    .appendTo(svgRoot)


if len(rectSetForPageL.rects):
  yRoofPageL = rectSetForPageL.frame.y0
  yFloorPageL = rectSetForPageL.frame.y1
  if xHingeRoofL is not None and yRoofPageL is not None:
    elmSpaceOverRoofPageL = svgDoc.createElement("rect") \
                                  .addClass("outside") \
                                  .setAttrByDict({"x": "0", \
                                                  "y": "0", \
                                                  "width": str(xHingeRoofL), \
                                                  "height": str(yRoofPageL)}) \
                                  .appendTo(svgRoot)
  if xHingeFloorL is not None and yFloorPageL is not None:
    elmSpaceUnderFloorPageL = svgDoc.createElement("rect") \
                                    .addClass("outside") \
                                    .setAttrByDict({"x": "0", \
                                                    "y": str(yFloorPageL), \
                                                    "width": str(xHingeFloorL), \
                                                    "height": str(hImg - yFloorPageL)}) \
                                    .appendTo(svgRoot)

# -----------------------------------------------------------------------------------

elmLines = filter(lambda elm: elm.hasAnyOfClasses(["line", "hline", "vline"]), \
                              svgDoc.getElementsByTagName("rect"))
elmBits = filter(lambda elm: \
                   elm.hasAnyOfClasses(["line", "vline", "hline", "glyph", "note", "page-numbers"]),
                 svgDoc.getElementsByTagName("rect"))

elmBits.sort(key=lambda elm: float(elm.getAttribute("height")) / float(elm.getAttribute("width")), reverse=True)

# find bits between notes
rectsNote = map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmNotes)
rectsLineAndBit = map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmBits + elmLines)

for aNote0 in rectsNote:
  aWindow0 = aNote0.createRectWithMargin(Opts["note-width"].max, 0)

  rectsNoteAround = filter(lambda r: aWindow0.hasOverlapWith(r), rectsNote)
  rectsLineAndBitAround = filter(lambda r: r.getWidth() <= Opts["vline-width"].max and aWindow0.hasOverlapWith(r), rectsLineAndBit)

  for aNote1 in rectsNoteAround:
    if aNote0 == aNote1:
      continue

    if not aWindow0.hasOverlapWith(aNote1):
      continue

    aGapRect = aNote0.createGapRectHWith(aNote1)
    if aGapRect is None:
      continue

    for aLineOrBit in rectsLineAndBitAround:
      if aGapRect.hasOverlapWith(aLineOrBit):
        aLineOrBit.svgElement.addClass("border")

elmBits = filter(lambda elm: elm.hasNoClass(), svgDoc.getElementsByTagName("rect"))
rectBits = map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmBits)

elmBorders = filter(lambda elm: elm.hasAnyOfClasses(["border", "vline"]), \
                    svgDoc.getElementsByTagName("rect"))
rectBorders = map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmBorders)

for aBorder in rectBorders:
  for aBit in rectBits:
    if aBit.getWidth() >= Opts["vline-width"].max:
      continue
    if not aBorder.hasOverlapX(aBit):
      continue
    if aBorder.getDistanceYFrom(aBit) > 3 * aBorder.getHeight():
      continue
    # print([aBorder.getCenterY(), aBit.getCenterY(), aBit.getHeight()])

    aBit.svgElement.addClass("vline-bit")

# ---
# make group of note rectangles
elmNotes = filter(lambda elm: elm.hasClass("note"), svgDoc.getElementsByTagName("rect"))
rectNotes2 = map(lambda elm: libRect.Rect() \
                                    .loadFromSvgElement(elm) \
                                    .createRectWithMargin(0 - Opts["vline-width"].max, 0) \
                                    .relateSvgElement(elm),
                 elmNotes)

noteSets = []
for r0 in rectNotes2:
  notInExistingSets = True
  for aSet in noteSets:
    # if any(map(lambda r1: r0.coverCenterX(r1), aSet.getRectList())):
    if any(map(lambda r1: r1.coverCenterX(r0), aSet.getRectList())):
      aSet.appendRect(r0)
      notInExistingSets = False

  if notInExistingSets:
    noteSets.append( libRect.RectSet().appendRect(r0) )

noteSets.sort(key=lambda aSet: 0 - aSet.updateFrame().frame.getCenterX())

iSet = 0
for aSet in noteSets:
  noteSetName = "noteset-" + str(iSet); 
  for aNote in aSet.getRectList():
    aNote.svgElement.addClass(noteSetName);

  elmFrame = aSet.updateFrame() \
                 .createSvgElement(svgDoc) \
                 .addClass("noteset-frame") \
                 .addClass("hidden")
  elmFrame.setAttribute("id", noteSetName)
  elmFrame.setAttribute("y", "0")
  elmFrame.setAttribute("height", str(hImg))
  aSet.frame.loadFromSvgElement(elmFrame)
  elmImg.parentNode.insertBefore(elmFrame, elmImg.nextSibling)

  iSet += 1

# remove "vline" class from the rectangle overlapping with noteSets
elmVline = filter(lambda elm: elm.hasClass("vline"), svgDoc.getElementsByTagName("rect"))
rectVline = map(lambda elm: libRect.Rect().loadFromSvgElement(elm), elmVline)

for iSet in range(0, len(noteSets)):
  aSet = noteSets[iSet]

  vlineAroundNoteSet = filter(lambda r: aSet.frame.coverCenterOf(r), rectVline)

  for b in vlineAroundNoteSet:
    b.svgElement.addClass("noteset-" + str(iSet))

    if aSet.frame.getVRangeX().covers(b.getVRangeX()):
      b.svgElement.addClass("not-vline").removeClass("vline").removeClass("border")
      continue

    # check nearest neighbor is not left or right
    nn0 = sorted(rectNotes2, key=lambda r: b.getDistanceFrom(r)).pop(0)

    # check nearest neighbor is not left or right
    nn1 = sorted(aSet.getRectList(), key=lambda r: b.getDistanceFrom(r)).pop(0)

    if nn0 != nn1:
      continue

    v = b.getVectorTo(nn1)
    theta = ((180 * math.atan2(v[1], v[0]) / math.pi) % 360)
    b.svgElement.setAttribute("data-length-nn1", str(nn1.getDistanceFrom(b)))
    b.svgElement.setAttribute("data-theta-nn1", str(int(theta)))
    b.svgElement.setAttribute("data-id-nn1", nn1.to_s())

    if b.hasOverlapX(nn1):
      b.svgElement.addClass("not-vline").removeClass("vline").removeClass("border")
      continue

# ---
# test noteSet is right or left
rectVline = filter(lambda r: r.svgElement.hasClass("vline"), rectVline)
# print(len(rectVline))

for iSet in range(0, len(noteSets)):
  aSet = noteSets[iSet]
 
  if iSet == 0:
    continue

  if iSet == len(noteSets) - 1:
    continue

  aSetR = noteSets[iSet - 1]
  aSetL = noteSets[iSet + 1]

  rangeXCurPlusR = aSet.frame.createToInclude(aSetR.frame).getVRangeX()
  rangeXCurPlusL = aSet.frame.createToInclude(aSetL.frame).getVRangeX()

  numVlineR = len(filter(lambda r: rangeXCurPlusR.covers(r.getVRangeX()), rectVline))
  numVlineL = len(filter(lambda r: rangeXCurPlusL.covers(r.getVRangeX()), rectVline))

  if numVlineL < numVlineR:
    aSet.svgElement.addClass("note-R")
  elif numVlineL > numVlineR:
    aSet.svgElement.addClass("note-L")

# ---
# set IDs
for elm in svgDoc.getElementsByTagName("rect"):
  r = libRect.Rect().loadFromSvgElement(elm)
  # elm.setAttribute("title", r.to_s())
  if len(elm.getAttribute("id")) == 0:
    elm.setAttribute("id", r.to_s())

# ---

for e in elmLines:
  svgRoot.appendChild(e)
for e in elmGlyphs:
  svgRoot.appendChild(e)
for e in elmPageNumbers:
  svgRoot.appendChild(e)
for e in elmBits:
  svgRoot.appendChild(e)

if xHingeRoof is not None and xHingeFloor is not None:
  svgDoc.createElement("line") \
        .addClass("hinge") \
        .setAttrByDict({ "x1": str(xHingeRoof), "y1": "0", "x2": str(xHingeFloor), "y2": str(hImg) }) \
        .appendTo(svgRoot)

# ---
# append script element
elmScript = svgDoc.createElement("script").appendTo(svgRoot)
if Opts["embed"]:
  fh = open("svg-show-id.js")
  svgShowIDJsText = fh.read()
  fh.close
  # elmScript.appendChild(svgDoc.createTextNode("//"))
  elmScript.appendChild(svgDoc.createCDATASection("\n" + svgShowIDJsText + "\n"))
  # elmScript.appendChild(svgDoc.createTextNode("//"))
else:
  elmScript.setAttribute("xlink:href", "svg-show-id.js")

#print(svgDoc.toxml())
if Opts["output"]:
  fh = open(Opts["output"], "w")
  fh.write(svgDoc.toprettyxml())
  fh.close
else:
  print(svgDoc.toprettyxml())
