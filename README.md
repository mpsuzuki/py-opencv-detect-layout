# Python scripting to detect the layout of the scanned image of Chinese woodblock prints.

This is a public snapshot of the testing scripts to detect the
layout structure of Chinese woodblock prints.

Currently, testing with Shuowen Jiezi Wuyun Yunpu (説文解字五音韻譜)
which a faximile reprint of the Songben was published as ISBN 9787514904192.

Supported by JSPS KAKENHI 19K12716.
