from xml.dom import minidom

# --

def minidom_element_getClassList(self):
  return self.getAttribute("class").split()

minidom.Element.getClassList = minidom_element_getClassList

# --

def minidom_element_hasNoClass(self):
  return len(self.getClassList()) == 0

minidom.Element.hasNoClass = minidom_element_hasNoClass

# --

def minidom_element_hasAnyOfClasses(self, clss):
  if 0 < len(set(clss) & set(self.getClassList())):
    return True
  else:
    return False

minidom.Element.hasAnyOfClasses = minidom_element_hasAnyOfClasses

# --

def minidom_element_hasClass(self, cls):
  return self.hasAnyOfClasses([cls])

minidom.Element.hasClass = minidom_element_hasClass

# --

def minidom_element_addClass(self, cls):
  if self.hasClass(cls):
    return self

  clss = self.getClassList()
  clss.append(cls)
  self.setAttribute("class", " ".join(clss))
  return self

minidom.Element.addClass = minidom_element_addClass

# --

def minidom_element_removeClass(self, cls):
  if not self.hasClass(cls):
    return self

  toks = []
  for c in self.getClassList():
    if c != cls:
      toks.append(c)

  self.setAttribute("class", " ".join(toks))
  return self

minidom.Element.removeClass = minidom_element_removeClass

# --

def minidom_element_setAttrByDict(self, d):
  for k in d.keys():
    self.setAttribute(k, d[k])

  return self

minidom.Element.setAttrByDict = minidom_element_setAttrByDict

# --
def minidom_element_appendTo(self, dest):
  dest.appendChild(self)
  return self

minidom.Element.appendTo = minidom_element_appendTo
