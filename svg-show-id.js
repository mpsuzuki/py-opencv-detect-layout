var setCallBacksToRects = function() {
  var addTipsID = function(evt) {
    var r = evt.srcElement;
    var id = r.getAttribute("id");
    var t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    r._tipsElement = t;

    // t.appendChild( document.createTextNode(id) );
    // t.appendChild( id );

    var noteSetNames = [];
    var clss = r.classList;
    clss = clss.toString();
    if (clss.length > 0)
    {
      clss = clss.split(/\s+/);
      noteSetNames = clss.filter(function(tok){return (tok.indexOf("noteset-") == 0);});
      clss.unshift(id);
      clss = clss.join("/");
    } else {
      clss = id;
    }

    {
      r._frameElements = []; 
      noteSetNames
        .forEach(
          function(n){
            var e = document.getElementById(n);
            if (e)
            {
              e.classList.remove("hidden");
              r._frameElements.push(e);
            };
          }
        );
    }

    if (evt.shiftKey) {
      clss = e.classList.toString().split(/\s+/).join("/");
    };

    t.appendChild( document.createTextNode(clss) );
    var bbox = r.getBBox();

    {
      r._lineCenterY = document.createElementNS("http://www.w3.org/2000/svg", "line");
      r._lineCenterY.setAttributeNS(null, "x1", bbox.x + (bbox.width / 2) );
      r._lineCenterY.setAttributeNS(null, "x2", bbox.x + (bbox.width / 2) );
      r._lineCenterY.setAttributeNS(null, "y1", 0 );
      r._lineCenterY.setAttributeNS(null, "y2", document.rootElement.getBBox().height);
      r.parentNode.insertBefore( r._lineCenterY, null );
    }

    // t.setAttributeNS(null, "x", r.getAttribute("x"));
    // t.setAttributeNS(null, "y", r.getAttribute("y"));
    t.setAttributeNS(null, "x", bbox.x + (bbox.width / 2) );
    t.setAttributeNS(null, "y", bbox.y + (bbox.height / 2) );
    t.setAttributeNS(null, "class", "id-text");

    console.log("add " + id);

    r.parentNode.insertBefore( t, null );
    if (bbox.x > (document.rootElement.getBBox().width / 2))
    {
      var bbox = t.getBBox();
      t.setAttributeNS(null, "x", bbox.x - bbox.width);
    };

    var bbox = t.getBBox();
    var back = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    back.setAttributeNS(null, "x", bbox.x);
    back.setAttributeNS(null, "y", bbox.y);
    back.setAttributeNS(null, "width", bbox.width);
    back.setAttributeNS(null, "height", bbox.height);
    back.setAttributeNS(null, "class", "id-text");
    t._backElement = back;

    t.parentNode.insertBefore( back, t );
  };

  var removeTipsID = function(evt) {
    var r = evt.srcElement;
    var y = r._lineCenterY;
    var t = r._tipsElement;
    var b = t._backElement;
    if (y)
    {
      y.parentNode.removeChild(y);
    };
    if (t)
    {
      console.log("remove " + t.textContent);
      b.parentNode.removeChild(b);
      t.parentNode.removeChild(t);
    };

    r._frameElements
     .forEach(    
       function(e){
         e.classList.add("hidden");
       }
     );
  };

  var elmRects = document.getElementsByTagName("rect");
  for (var i = 0; i < elmRects.length; i += 1)
  {
    var e = elmRects[i];
    e.addEventListener("mouseover", addTipsID);
    e.addEventListener("mouseout", removeTipsID);
  };
};
setCallBacksToRects();
