#!/usr/bin/env python

import re

def getOpts(Opts, argv, optsBoolean):
  if not "args" in Opts.keys():
    Opts["args"] = []

  idx = 1
  while idx < len(argv):
    arg = argv[idx]
    if arg.startswith("-"):
      arg = re.sub(r"^-+", "", arg)
      if arg in optsBoolean:
        Opts[arg] = True
      elif arg.find("=") > 0:
        k, v = arg.split("=", 2)
        Opts[k] = v
      else:
        k = arg
        v = argv[idx + 1]
        Opts[k] = v
        idx += 1
    else:
      Opts["args"].append(arg)

    idx += 1

  return Opts
